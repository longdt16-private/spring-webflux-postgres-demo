package com.jabc.demo;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
public class ReactiveSpringPostgresqlDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveSpringPostgresqlDemoApplication.class, args);
	}

}
