package com.jabc.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class ProductHandler {

    @Autowired
    private ProductRepository productRepository;

    public Mono<ServerResponse> getAllProducts(ServerRequest request) {
        Flux<Product> products = productRepository.findAll();
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(products, Product.class);
    }

    public Mono<ServerResponse> getProduct(ServerRequest request) {
        Integer id = Integer.valueOf(request.pathVariable("id"));
        return productRepository.findById(id)
                .flatMap(product -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(Mono.just(product), Product.class)).switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> addNewProduct(ServerRequest request) {
        Mono<Product> ProductMono = request.bodyToMono(Product.class);
        Mono<Product> newProduct = ProductMono.flatMap(productRepository::save);
        return ServerResponse.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(newProduct, Product.class);
    }

    public Mono<ServerResponse> updateProduct(ServerRequest request) {
        Integer id = Integer.valueOf(request.pathVariable("id"));
        Mono<Product> productMono = request.bodyToMono(Product.class);
        return productRepository.findById(id)
                .flatMap(product -> productMono.flatMap(product1 -> {
                    product.setName(product1.getName());
                    Mono<Product> updatedProduct = productRepository.save(product);
                    return ServerResponse.ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(updatedProduct, Product.class);
                }));
    }

    public Mono<ServerResponse> deleteProduct(ServerRequest request) {
        Integer id = Integer.valueOf(request.pathVariable("id"));
        return productRepository.findById(id)
                .flatMap(Product -> productRepository
                        .delete(Product)
                        .then(ServerResponse.ok().build()))
                .switchIfEmpty(ServerResponse.notFound().build());
    }
}
