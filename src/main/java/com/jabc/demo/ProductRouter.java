package com.jabc.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration
public class ProductRouter {

    @Bean
    public RouterFunction<ServerResponse> route(ProductHandler productHandler) {
        return RouterFunctions.route(
                        GET("/products").and(RequestPredicates
                                .accept(MediaType.APPLICATION_JSON)), productHandler::getAllProducts)
                .andRoute(GET("/products/{id}").and(RequestPredicates
                                .accept(MediaType.APPLICATION_JSON)),
                        productHandler::getProduct)
                .andRoute(POST("/products").and(RequestPredicates
                                .accept(MediaType.APPLICATION_JSON)),
                        productHandler::addNewProduct)
                .andRoute(PUT("/products/{id}").and(RequestPredicates
                        .accept(MediaType.APPLICATION_JSON)), productHandler::updateProduct)
                .andRoute(DELETE("/products/{id}").and(RequestPredicates
                        .accept(MediaType.APPLICATION_JSON)), productHandler::deleteProduct);
    }
}

