package com.jabc.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = ProductRouter.class)
@Import(ProductHandler.class)
public class ProductRouterTest
{
    @MockBean
    ProductRepository productRepository;

    @Autowired
    private WebTestClient webClient;

    @Test
    void testCreateProduct() {
        Product product = new Product();
        product.setId(1);
        product.setName("Test");

        Mockito.when(productRepository.save(product)).thenReturn(Mono.just(product));

        webClient.post()
                .uri("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(product))
                .exchange()
                .expectStatus().isCreated();

        Mockito.verify(productRepository, times(1)).save(product);
    }

    @Test
    void testGetProductById()
    {
        Product product = new Product();
        product.setId(100);
        product.setName("Test");

        Mockito
                .when(productRepository.findById(100))
                .thenReturn(Mono.just(product));

        webClient.get().uri("/products/{id}", 100)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.name").isNotEmpty()
                .jsonPath("$.id").isEqualTo(100)
                .jsonPath("$.name").isEqualTo("Test");

        Mockito.verify(productRepository, times(1)).findById(100);
    }
}
